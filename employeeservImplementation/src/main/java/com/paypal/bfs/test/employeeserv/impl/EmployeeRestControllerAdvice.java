package com.paypal.bfs.test.employeeserv.impl;

import com.paypal.bfs.test.employeeserv.error.EntityNotFoundException;
import com.paypal.bfs.test.employeeserv.error.ErrorResponse;
import com.paypal.bfs.test.employeeserv.error.ValidationFailedException;
import com.paypal.bfs.test.employeeserv.validation.base.ValidationResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
@Slf4j
public class EmployeeRestControllerAdvice {

    private static final String GENERIC_ERROR_MESSAGE = "Unknown error occurred. Please contact customer support.";

    @ExceptionHandler(ValidationFailedException.class)
    public ErrorResponse handle(HttpServletRequest req, HttpServletResponse res, ValidationFailedException validationFailedException) {
        ValidationResult validationResult = validationFailedException.getValidationResult();
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorMessage(validationResult.getFailureReason());
        errorResponse.setAdditionalInformation(validationResult.getAdditionalInformation());
        switch (validationResult.getValidator()) {
            case EMPLOYEE_EXISTENCE_VALIDATOR:
                errorResponse.setErrorCode(HttpServletResponse.SC_NOT_FOUND);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                break;
            default:
                errorResponse.setErrorCode(HttpServletResponse.SC_BAD_REQUEST);
                res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                break;
        }
        return errorResponse;
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ErrorResponse handle(HttpServletRequest req, HttpServletResponse res, EntityNotFoundException entityNotFoundException) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorMessage(entityNotFoundException.getMessage());
        errorResponse.setErrorCode(HttpServletResponse.SC_NOT_FOUND);
        res.setStatus(HttpServletResponse.SC_NOT_FOUND);
        return errorResponse;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handle(Exception e) {
        log.error("Unknown error", e);
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        errorResponse.setErrorMessage(GENERIC_ERROR_MESSAGE);
        return errorResponse;
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    public ErrorResponse handle(HttpServletRequest req, HttpServletResponse res, MissingRequestHeaderException missingRequestHeaderException) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorMessage(missingRequestHeaderException.getMessage());
        errorResponse.setErrorCode(HttpServletResponse.SC_BAD_REQUEST);
        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        return errorResponse;
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ErrorResponse handle(HttpServletRequest req, HttpServletResponse res, HttpMessageNotReadableException httpMessageNotReadableException) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorMessage(httpMessageNotReadableException.getMessage());
        errorResponse.setErrorCode(HttpServletResponse.SC_BAD_REQUEST);
        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        return errorResponse;
    }
}
