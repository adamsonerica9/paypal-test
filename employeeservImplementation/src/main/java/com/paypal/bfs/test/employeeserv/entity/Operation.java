package com.paypal.bfs.test.employeeserv.entity;

public enum Operation {
    CREATE, UPDATE, DELETE
}
