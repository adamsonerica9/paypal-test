package com.paypal.bfs.test.employeeserv.entity;

import com.paypal.bfs.test.employeeserv.api.model.Employee;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.*;
import java.time.Instant;
import java.util.Date;


@Table(name = "employee")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
/**
 * Note on type of date of birth: Schema generator is little inflexible. format:date results in string type. So making javax validations difficult.
 * Keeping time part optional
 */
public class EmployeeEO {

    @Id
    @SequenceGenerator(name = "employee_seq", sequenceName = "employee_seq", allocationSize = 100, initialValue = 100000)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "employee_seq")
    @Column(name = "id")
    public Integer id;

    @Embedded
    private AddressEO address;

    @Column(name = "first_name", length = 255, nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 255, nullable = false)
    private String lastName;

    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @CreationTimestamp
    private Instant createdAt;

    @UpdateTimestamp
    private Instant updatedAt;

    @Column(name = "is_deleted")
    private boolean isDeleted = false;


    public static EmployeeEO from(Employee employee) {
        AddressEO address = AddressEO.from(employee.getAddress());
        return EmployeeEO.builder()
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .dateOfBirth(employee.getDateOfBirth())
                .address(address)
                .build();
    }

}
