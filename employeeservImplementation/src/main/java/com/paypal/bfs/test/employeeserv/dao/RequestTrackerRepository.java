package com.paypal.bfs.test.employeeserv.dao;

import com.paypal.bfs.test.employeeserv.entity.Entity;
import com.paypal.bfs.test.employeeserv.entity.Operation;
import com.paypal.bfs.test.employeeserv.entity.RequestTrackerEO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestTrackerRepository extends JpaRepository<RequestTrackerEO, Integer> {

    @Query("select rt from RequestTrackerEO rt where rt.requestId = :requestId and rt.entity = :entity and rt.operation = :operation")
    RequestTrackerEO getBy(String requestId, Entity entity, Operation operation);
}
