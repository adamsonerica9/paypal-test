package com.paypal.bfs.test.employeeserv.dao;

import com.paypal.bfs.test.employeeserv.entity.EmployeeEO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeEO, Integer> {

    @Query("select emp from EmployeeEO emp where emp.id = :id and emp.isDeleted = false")
    EmployeeEO getEmployeeById(Integer id);
}
