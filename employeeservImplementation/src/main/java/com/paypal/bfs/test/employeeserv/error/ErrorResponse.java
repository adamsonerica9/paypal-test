package com.paypal.bfs.test.employeeserv.error;

import lombok.*;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ErrorResponse {

    private String errorMessage;

    private Integer errorCode;

    private Map<String, Object> additionalInformation;
}
