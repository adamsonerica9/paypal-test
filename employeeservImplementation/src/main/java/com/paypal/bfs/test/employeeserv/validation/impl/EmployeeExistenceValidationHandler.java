package com.paypal.bfs.test.employeeserv.validation.impl;

import com.paypal.bfs.test.employeeserv.dao.EmployeeRepository;
import com.paypal.bfs.test.employeeserv.entity.EmployeeEO;
import com.paypal.bfs.test.employeeserv.validation.base.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;

@Scope(scopeName = "prototype")
@Component
@Slf4j
public class EmployeeExistenceValidationHandler implements ValidationHandler {

    private static final String EMPLOYEE_ID = "employeeId";

    private ValidationHandler nextHandlerToInvoke;

    private EmployeeRepository employeeRepository;

    @Autowired
    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public ValidationResult handle(ValidationContext validationContext) {
        Map<String, Object> additionalInformation = validationContext.getAdditionalInformation();
        String entityId = (String) additionalInformation.get(EMPLOYEE_ID);
        Integer employeeId = null;
        try {
            employeeId = Integer.parseInt(entityId);
        } catch (Exception e) {
            log.error("Invalid employee id detected. Treating it as not found.");
            return ValidationResult.failed()
                    .at(getValidator())
                    .dueTo(String.format(ValidationErrorConstants.EMPLOYEE_NOT_FOUND, entityId));
        }
        EmployeeEO employee = employeeRepository.getEmployeeById(employeeId);
        if (employee == null) {
            log.error("Invalid employee id. Id received : {}", entityId);
            return ValidationResult.failed()
                    .at(getValidator())
                    .dueTo(String.format(ValidationErrorConstants.EMPLOYEE_NOT_FOUND, entityId));
        }
        if (nextHandlerToInvoke != null) {
            return nextHandlerToInvoke.handle(validationContext);
        }
        return ValidationResult.success();
    }

    @Override
    public void setNext(ValidationHandler validationHandler) {
        nextHandlerToInvoke = validationHandler;
    }

    @Override
    public Validator getValidator() {
        return Validator.EMPLOYEE_EXISTENCE_VALIDATOR;
    }
}
