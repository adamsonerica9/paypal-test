package com.paypal.bfs.test.employeeserv.validation.impl;

import com.paypal.bfs.test.employeeserv.dao.RequestTrackerRepository;
import com.paypal.bfs.test.employeeserv.entity.Entity;
import com.paypal.bfs.test.employeeserv.entity.Operation;
import com.paypal.bfs.test.employeeserv.entity.RequestTrackerEO;
import com.paypal.bfs.test.employeeserv.validation.base.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope(scopeName = "prototype")
@Component
@Slf4j
public class RequestReplayValidationHandler implements ValidationHandler {

    private ValidationHandler nextHandlerToInvoke;

    private RequestTrackerRepository requestTrackerRepository;

    @Autowired
    public void setRequestTrackerRepository(RequestTrackerRepository requestTrackerRepository) {
        this.requestTrackerRepository = requestTrackerRepository;
    }

    @Override
    public ValidationResult handle(ValidationContext validationContext) {
        String requestId = validationContext.getRequestId();
        Entity entity = validationContext.getEntityType();
        Operation operation = validationContext.getOperation();
        RequestTrackerEO previousRequest = requestTrackerRepository.getBy(requestId, entity, operation);
        if (previousRequest != null) {
            log.error("Replay detected. Request id : {}", requestId);
            return ValidationResult.failed()
                    .at(getValidator())
                    .dueTo(ValidationErrorConstants.REQUEST_REPLAYED);
        }
        if (nextHandlerToInvoke != null) {
            return nextHandlerToInvoke.handle(validationContext);
        }
        return ValidationResult.success();
    }

    @Override
    public void setNext(ValidationHandler validationHandler) {
        nextHandlerToInvoke = validationHandler;
    }

    @Override
    public Validator getValidator() {
        return Validator.REQUEST_REPLAY_VALIDATOR;
    }
}
