package com.paypal.bfs.test.employeeserv.validation.base;

//All implementations will be prototype scope to prevent concurrency issues during validator chain building
public interface ValidationHandler {

    ValidationResult handle(ValidationContext validationContext);

    void setNext(ValidationHandler validationHandler);

    Validator getValidator();
}
