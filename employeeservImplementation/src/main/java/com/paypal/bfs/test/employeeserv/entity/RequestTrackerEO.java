package com.paypal.bfs.test.employeeserv.entity;

import lombok.*;

import javax.persistence.*;

@Table(name = "request_tracker")
@javax.persistence.Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class RequestTrackerEO {

    @Id
    @SequenceGenerator(name = "req_tracker_seq", sequenceName = "req_tracker_seq", allocationSize = 100, initialValue = 100000)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "req_tracker_seq")
    @Column(name = "id")
    public Integer id;

    @Column(name = "request_id", length = 512, nullable = false)
    private String requestId;

    @Enumerated(EnumType.STRING)
    @Column(name = "entity")
    private Entity entity;

    @Enumerated(EnumType.STRING)
    @Column(name = "operation")
    private Operation operation;

    public static RequestTrackerEO from(String requestId, Entity entity, Operation operation) {
        return RequestTrackerEO.builder()
                .entity(entity)
                .operation(operation)
                .requestId(requestId)
                .build();
    }

}
