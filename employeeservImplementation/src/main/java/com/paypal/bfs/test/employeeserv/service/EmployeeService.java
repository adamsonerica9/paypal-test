package com.paypal.bfs.test.employeeserv.service;

import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.dao.EmployeeRepository;
import com.paypal.bfs.test.employeeserv.dao.RequestTrackerRepository;
import com.paypal.bfs.test.employeeserv.entity.*;
import com.paypal.bfs.test.employeeserv.error.EntityNotFoundException;
import com.paypal.bfs.test.employeeserv.error.ErrorMessageConstants;
import com.paypal.bfs.test.employeeserv.error.ValidationFailedException;
import com.paypal.bfs.test.employeeserv.validation.base.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class EmployeeService {

    private static final String EMPLOYEE_ID = "employeeId";

    private EmployeeValidatorFactory validatorFactory;

    private EmployeeRepository employeeRepository;

    private RequestTrackerRepository requestTrackerRepository;

    @Autowired
    public void setRequestTrackerRepository(RequestTrackerRepository requestTrackerRepository) {
        this.requestTrackerRepository = requestTrackerRepository;
    }

    @Autowired
    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Autowired
    public void setValidatorFactory(EmployeeValidatorFactory validatorFactory) {
        this.validatorFactory = validatorFactory;
    }


    /**
     *
     * Note on deduplication: The list of fields that we are accepting now is not sufficient to prevent duplicate resource creation.
     * So I've only implemented measures to detect replays. If we can accept SSN or email id in the future, we can provide resource level deduplication.
     */
    @Transactional
    public Integer createEmployee(Employee employee, String requestId) {
        runEmployeeCreateValidation(employee, requestId);
        EmployeeEO employeeToCreate = EmployeeEO.from(employee);
        RequestTrackerEO requestTracker = RequestTrackerEO.from(requestId, Entity.EMPLOYEE, Operation.CREATE);
        employeeToCreate = employeeRepository.save(employeeToCreate);
        requestTrackerRepository.save(requestTracker);
        return employeeToCreate.getId();
    }

    public Employee getEmployeeById(String id) {
        runFetchEmployeeValidation(id);
        EmployeeEO employee = employeeRepository.getEmployeeById(Integer.valueOf(id));
        if (employee == null) {
            log.error("Validation passed but the control is here. It's possible that the entity was deleted right after validation.");
            throw new EntityNotFoundException(String.format(ErrorMessageConstants.ENTITY_NOT_FOUND, id));
        }

        return getEmployeeFrom(employee);
    }

    private Employee getEmployeeFrom(EmployeeEO employeeEO) {
        Employee employee = new Employee();
        AddressEO addressEO = employeeEO.getAddress();
        Address address = new Address();
        address.setLine1(addressEO.getLine1());
        address.setLine2(addressEO.getLine2());
        address.setCity(addressEO.getCity());
        address.setState(addressEO.getState());
        address.setCountry(addressEO.getCountry());
        address.setZipCode(addressEO.getZipCode());
        employee.setAddress(address);
        employee.setId(employeeEO.getId());
        employee.setDateOfBirth(employeeEO.getDateOfBirth());
        employee.setFirstName(employeeEO.getFirstName());
        employee.setLastName(employeeEO.getLastName());
        return employee;
    }

    private void runFetchEmployeeValidation(String id) {
        ValidationHandler fetchEmployeeValidator = validatorFactory.getEmployeeReadValidator();
        Map<String, Object> additionalInformation = new HashMap<>();
        additionalInformation.put(EMPLOYEE_ID, id);
        ValidationContext validationContext = ValidationContext.builder()
                .additionalInformation(additionalInformation)
                .entityType(Entity.EMPLOYEE)
                .build();
        ValidationResult validationResult = fetchEmployeeValidator.handle(validationContext);
        if (validationResult.getValidationStatus() == ValidationStatus.FAILED) {
            log.error("Employee fetch failed. Validator result : {}", validationResult);
            throw new ValidationFailedException(validationResult);
        }

    }

    private void runEmployeeCreateValidation(Employee employee, String requestId) {
        ValidationHandler employeeCreationValidator = validatorFactory.getEmployeeCreationValidator();
        ValidationContext validationContext = ValidationContext.builder()
                .entity(employee)
                .entityType(Entity.EMPLOYEE)
                .operation(Operation.CREATE)
                .requestId(requestId)
                .build();
        ValidationResult validationResult = employeeCreationValidator.handle(validationContext);
        if (validationResult.getValidationStatus() == ValidationStatus.FAILED) {
            log.error("Employee creation failed. Validator result : {}", validationResult);
            throw new ValidationFailedException(validationResult);
        }
    }
}
