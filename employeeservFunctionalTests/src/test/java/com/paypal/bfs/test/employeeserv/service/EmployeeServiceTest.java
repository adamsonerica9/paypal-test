package com.paypal.bfs.test.employeeserv.service;

import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.dao.EmployeeRepository;
import com.paypal.bfs.test.employeeserv.dao.RequestTrackerRepository;
import com.paypal.bfs.test.employeeserv.entity.EmployeeEO;
import com.paypal.bfs.test.employeeserv.entity.RequestTrackerEO;
import com.paypal.bfs.test.employeeserv.error.EntityNotFoundException;
import com.paypal.bfs.test.employeeserv.error.ValidationFailedException;
import com.paypal.bfs.test.employeeserv.validation.base.EmployeeValidatorFactory;
import com.paypal.bfs.test.employeeserv.validation.base.ValidationContext;
import com.paypal.bfs.test.employeeserv.validation.base.ValidationHandler;
import com.paypal.bfs.test.employeeserv.validation.base.ValidationResult;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(BlockJUnit4ClassRunner.class)
@Slf4j
public class EmployeeServiceTest {

    private EmployeeValidatorFactory validatorFactory;

    private EmployeeRepository employeeRepository;

    private RequestTrackerRepository requestTrackerRepository;

    private EmployeeService employeeService;

    private ValidationHandler employeeCreateValidationHandler;

    private ValidationHandler employeeReadValidationHandler;

    @Before
    public void before() {
        validatorFactory = mock(EmployeeValidatorFactory.class);
        employeeRepository = mock(EmployeeRepository.class);
        requestTrackerRepository = mock(RequestTrackerRepository.class);
        employeeCreateValidationHandler = mock(ValidationHandler.class);
        employeeReadValidationHandler = mock(ValidationHandler.class);
        employeeService = new EmployeeService();
        employeeService.setEmployeeRepository(employeeRepository);
        employeeService.setRequestTrackerRepository(requestTrackerRepository);
        employeeService.setValidatorFactory(validatorFactory);
    }

    @Test
    public void employeeCreationShouldFailThereIsValidationErrors() {
        when(employeeCreateValidationHandler.handle(any(ValidationContext.class)))
                .thenReturn(ValidationResult.failed());
        when(validatorFactory.getEmployeeCreationValidator()).thenReturn(employeeCreateValidationHandler);
        Employee employeeCreateRequest = getValidEmployeeCreateRequest();
        try {
            employeeService.createEmployee(employeeCreateRequest, "req-123");
            Assert.fail("Exception expected here.");
        } catch (Exception e) {
            Assert.assertTrue(e instanceof ValidationFailedException);
        }

    }

    @Test
    public void employeeCreationShouldSucceedIfThereIsNoValidationErrors() {
        Employee employeeCreateRequest = getValidEmployeeCreateRequest();
        EmployeeEO employeeEO = EmployeeEO.from(employeeCreateRequest);
        employeeEO.setId(100001);
        when(employeeRepository.save(any(EmployeeEO.class))).thenReturn(employeeEO);
        when(requestTrackerRepository.save(any(RequestTrackerEO.class))).thenReturn(new RequestTrackerEO());
        when(employeeCreateValidationHandler.handle(any(ValidationContext.class)))
                .thenReturn(ValidationResult.success());
        when(validatorFactory.getEmployeeCreationValidator()).thenReturn(employeeCreateValidationHandler);
        Integer employeeId = employeeService.createEmployee(employeeCreateRequest, "req-123");
        Assert.assertEquals(100001, (int) employeeId);
    }

    @Test
    public void employeeReadShouldFailIfThereIsValidationErrors() {
        when(employeeReadValidationHandler.handle(any(ValidationContext.class)))
                .thenReturn(ValidationResult.failed());
        when(validatorFactory.getEmployeeReadValidator()).thenReturn(employeeReadValidationHandler);
        try {
            employeeService.getEmployeeById("100001");
            Assert.fail("Exception expected here.");
        } catch (Exception e) {
            Assert.assertTrue(e instanceof ValidationFailedException);
        }
    }

    @Test
    public void employeeReadShouldFailIfTheValidationSucceedsButEntityDeletedShortlyAfterThat() {
        when(employeeRepository.getEmployeeById(anyInt())).thenReturn(null);
        when(employeeReadValidationHandler.handle(any(ValidationContext.class)))
                .thenReturn(ValidationResult.success());
        when(validatorFactory.getEmployeeReadValidator()).thenReturn(employeeReadValidationHandler);
        try {
            employeeService.getEmployeeById("100001");
            Assert.fail("Exception expected here.");
        } catch (Exception e) {
            Assert.assertTrue(e instanceof EntityNotFoundException);
        }
    }

    @Test
    public void employeeReadShouldSucceedIfThereIsNoValidationErrors() {
        Employee employeeCreateRequest = getValidEmployeeCreateRequest();
        EmployeeEO employeeEO = EmployeeEO.from(employeeCreateRequest);
        employeeEO.setId(100001);
        when(employeeRepository.getEmployeeById(anyInt())).thenReturn(employeeEO);
        when(employeeReadValidationHandler.handle(any(ValidationContext.class)))
                .thenReturn(ValidationResult.success());
        when(validatorFactory.getEmployeeReadValidator()).thenReturn(employeeReadValidationHandler);
        Employee employee = employeeService.getEmployeeById("100001");
        Assert.assertNotNull(employee);
        Assert.assertEquals(100001, (int) employee.getId());
    }

    private Employee getValidEmployeeCreateRequest() {
        String dateOfBirthString = "1900-01-01";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dataOfBirth = null;
        try {
            dataOfBirth = formatter.parse(dateOfBirthString);
        } catch (Exception e) {
            log.error("Error converting date", e);
        }
        Address address = new Address();
        address.setLine1("line1");
        address.setLine2("line2");
        address.setZipCode("007007");
        address.setState("KA");
        address.setCountry("IN");
        address.setCity("Bengaluru");
        Employee employee = new Employee();
        employee.setFirstName("ben");
        employee.setLastName("10");
        employee.setDateOfBirth(dataOfBirth);
        employee.setAddress(address);
        return employee;
    }

}
