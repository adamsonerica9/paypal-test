package com.paypal.bfs.test.employeeserv.validation.impl;

import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.entity.Entity;
import com.paypal.bfs.test.employeeserv.entity.Operation;
import com.paypal.bfs.test.employeeserv.validation.base.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RunWith(BlockJUnit4ClassRunner.class)
@Slf4j
public class EmployeePayloadValidationHandlerTest {

    private static String NULL_VALIDATION_MESSAGE = "must not be null";

    private EmployeePayloadValidationHandler employeePayloadValidationHandler;


    @Before
    public void before() {
        employeePayloadValidationHandler = new EmployeePayloadValidationHandler();
    }

    @Test
    public void validationShouldFailIfTheNonNullValidationsDoesNotHold() {
        Employee employee = getValidEmployeeCreateRequest();
        employee.setFirstName(null);
        ValidationContext validationContext = ValidationContext.builder()
                .requestId("asdettnvhshwq")
                .operation(Operation.CREATE)
                .entityType(Entity.EMPLOYEE)
                .entity(employee)
                .build();
        ValidationResult validationResult = employeePayloadValidationHandler.handle(validationContext);
        Assert.assertEquals(ValidationStatus.FAILED, validationResult.getValidationStatus());
        Assert.assertEquals(Validator.EMPLOYEE_PAYLOAD_VALIDATOR, validationResult.getValidator());
        Assert.assertEquals(ValidationErrorConstants.PAYLOAD_VALIDATION_FAILED, validationResult.getFailureReason());
        Map<String, Object> additionalInformation = validationResult.getAdditionalInformation();
        String errorReason = (String) additionalInformation.get("firstName");
        Assert.assertEquals(NULL_VALIDATION_MESSAGE, errorReason);

        //testing for one more field
        employee = getValidEmployeeCreateRequest();
        employee.getAddress().setState(null);
        validationContext = ValidationContext.builder()
                .requestId("asdettnvhshwq")
                .operation(Operation.CREATE)
                .entityType(Entity.EMPLOYEE)
                .entity(employee)
                .build();
        validationResult = employeePayloadValidationHandler.handle(validationContext);
        Assert.assertEquals(ValidationStatus.FAILED, validationResult.getValidationStatus());
        Assert.assertEquals(Validator.EMPLOYEE_PAYLOAD_VALIDATOR, validationResult.getValidator());
        Assert.assertEquals(ValidationErrorConstants.PAYLOAD_VALIDATION_FAILED, validationResult.getFailureReason());
        additionalInformation = validationResult.getAdditionalInformation();
        errorReason = (String) additionalInformation.get("address.state");
        Assert.assertEquals(NULL_VALIDATION_MESSAGE, errorReason);
    }

    @Test
    public void validationShouldNotFailIfLine2IsNotPresentSinceItIsAOptionalField() {
        Employee employee = getValidEmployeeCreateRequest();
        employee.getAddress().setLine2(null);
        ValidationContext validationContext = ValidationContext.builder()
                .requestId("asdettnvhshwq")
                .operation(Operation.CREATE)
                .entityType(Entity.EMPLOYEE)
                .entity(employee)
                .build();
        ValidationResult validationResult = employeePayloadValidationHandler.handle(validationContext);
        Assert.assertEquals(ValidationStatus.SUCCESS, validationResult.getValidationStatus());
    }

    @Test
    public void validationShouldSucceedIfAllTheRequiredFieldsArePresent() {
        Employee employee = getValidEmployeeCreateRequest();
        ValidationContext validationContext = ValidationContext.builder()
                .requestId("asdettnvhshwq")
                .operation(Operation.CREATE)
                .entityType(Entity.EMPLOYEE)
                .entity(employee)
                .build();
        ValidationResult validationResult = employeePayloadValidationHandler.handle(validationContext);
        Assert.assertEquals(ValidationStatus.SUCCESS, validationResult.getValidationStatus());
    }

    private Employee getValidEmployeeCreateRequest() {
        String dateOfBirthString = "1900-01-01";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dataOfBirth = null;
        try {
            dataOfBirth = formatter.parse(dateOfBirthString);
        } catch (Exception e) {
            log.error("Error converting date", e);
        }
        Address address = new Address();
        address.setLine1("line1");
        address.setLine2("line2");
        address.setZipCode("007007");
        address.setState("KA");
        address.setCountry("IN");
        address.setCity("Bengaluru");
        Employee employee = new Employee();
        employee.setFirstName("ben");
        employee.setLastName("10");
        employee.setDateOfBirth(dataOfBirth);
        employee.setAddress(address);
        return employee;
    }

}
